.PHONY: language_tests clean pdf latex

latex:
	bnfc --latex src/Syntax/Normal.cf -o doc/Syntax/

pdf:
	make latex
	latexmk -output-directory=doc/Syntax/ -pdflatex doc/Syntax/Normal.tex

build:
	make build_grammar
	stack --local-bin-path target install --test

build_grammar:
	rm -rf src/Normal
	bnfc -d --functor src/Syntax/Normal.cf
	rm Normal/Test.hs
	mv Normal src/

language_tests:
	./run_language_tests

clean:
	rm -rf src/Normal

build_in_docker:
	docker build -f Dockerfile.build -t aid2_build:latest .
	docker run -d --name aid2_build aid2_build:latest
	mkdir -p target
	docker cp aid2_build:/app/target/aid2 target/aid2
	docker rm -f aid2_build
