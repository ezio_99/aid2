{-# OPTIONS_GHC -Wall #-}

module TypeOfPureSpec where

import AID2.Data
import AID2.Infer.Context
import AID2.Infer.Data
import AID2.Infer.Helpers
import AID2.Infer.TypeOfPure
import qualified Data.Set as Set
import Gen
import Test.Hspec
import Test.QuickCheck

baseContext :: ST
baseContext = ST [predef] [] [] Set.empty Set.empty

baseContextWithTypeVars :: ST
baseContextWithTypeVars = ST [predef] [] freshTypeVars Set.empty Set.empty

spec :: Spec
spec = do
  describe "AID2.Infer.TypeOfPure.typeOfListPure" $ do
    it "for []" $
      do
        fst $ typeOfListPure baseContextWithTypeVars Nothing []
        `shouldBe` Right (ListType' $ TypeVar Nothing "a1")

    it "for [t]" $
      property $
        \p@(t, _) ->
          do
            fst $ typeOfListPure baseContext Nothing [p]
            `shouldBe` Right (ListType' t)

    it "for [t, c]" $
      property $
        \t1 -> do
          t2 <- genTVNotEqualTo t1
          let c = Constraint Nothing t1 t2
          typeOfListPure baseContext Nothing [(t1, Nothing), (t2, Nothing)]
            `shouldBe` ( Right (ListType' t1),
                         baseContext
                           { getConstraints = [c],
                             getConstrainedTypeVars = Set.fromList $ typeVarsFromConstraint c
                           }
                       )
