{-# OPTIONS_GHC -Wall #-}

module UnifySpec where

import AID2.Infer.Data
import AID2.Data
import AID2.Infer.Unify
import Control.Monad.Except (runExcept)
import Gen
import Test.Hspec
import Test.QuickCheck
import Data.Either

sub :: GeneralType -> GeneralType -> [Constraint] -> Either Error [Constraint]
sub t1 t2 cs = runExcept (subst Nothing t1 t2 cs)

occurs :: GeneralType -> GeneralType -> Either Error ()
occurs tv target = runExcept (occursCheck Nothing (Sub tv) (Target target))

spec :: Spec
spec = do
  describe "AID2.Infer.Unify.subst" $ do
    it "for [t=t]" $
      property $
        \t -> do
          sub t t [] `shouldBe` Right []

    it "for [t=c]" $
      property $
        \c -> do
          let t = TypeVar Nothing "a-1"
          let res = Right []
          sub t c [] `shouldBe` res
          sub c t [] `shouldBe` res

    it "for [t=c, ...]" $
      property $
        \c -> do
          let t = TypeVar Nothing "a-1"
          t1 <- genWithoutTV
          t2 <- genWithoutTV
          cNoTV <- genWithoutTV
          let cs = [Constraint Nothing t t1, Constraint Nothing t t2]
          sub t c cs `shouldBe` Right [Constraint Nothing c t1, Constraint Nothing c t2]
          sub cNoTV t cs `shouldBe` Right [Constraint Nothing cNoTV t1, Constraint Nothing cNoTV t2]

    it "for [t=c, ..., x=y]" $
      property $
        \a -> do
          let t = TypeVar Nothing $ "a-" ++ a
          let t1 = TypeVar Nothing "b-1"
          let t2 = TypeVar Nothing "c-1"
          c <- genWithoutTV
          let cs = [Constraint Nothing t t1, Constraint Nothing t t2]
          let res = Right [Constraint Nothing c t1, Constraint Nothing c t2]
          sub t c cs `shouldBe` res
          sub c t cs `shouldBe` res

    it "for [[x]=[y]]" $
      property $
        \x -> do
          y <- genGTNotEqualTo x
          let xl = ListType' x
          let yl = ListType' y
          let res = Right [Constraint Nothing x y]
          sub xl yl [] `shouldBe` res
          sub yl xl [] `shouldBe` res

    it "for [<...> -> t1=<...> -> t2]" $
      property $
        \retType1 -> do
          args1 <- genGTList
          args2 <- genGTList
          retType2 <- genGTNotEqualTo retType1
          let f1 = FuncType' args1 retType1
          let f2 = FuncType' args2 retType2
          let res = Right $ (fmap (\(a, b) -> Constraint Nothing a b) $ zip args1 args2) ++ [Constraint Nothing retType1 retType2]
          sub f1 f2 [] `shouldBe` res
          sub f2 f1 [] `shouldBe` res

    it "for [x=[x]]" $
      property $
        \(TV t) -> do
          let t1 = TypeVar Nothing t
          let t2 = ListType' t1
          isLeft (sub t1 t2 []) `shouldBe` True
          isLeft (sub t2 t1 []) `shouldBe` True

  describe "AID2.Infer.Unify.occursCheck" $ do
    it "for t=t" $
      property $
        \t -> do
          isLeft (occurs t t) `shouldBe` False

    it "for t=[t]" $
      property $
        \(TV t) -> do
          let t1 = TypeVar Nothing t
          isLeft (occurs t1 (ListType' t1)) `shouldBe` True

    it "for t=<..>->.." $
      property $
        \(TV t) -> do
          let t1 = TypeVar Nothing t
          rt <- genGTNotEqualTo t1
          isLeft (occurs t1 (FuncType' [t1] rt)) `shouldBe` True
          isLeft (occurs t1 (FuncType' [rt] t1)) `shouldBe` True
