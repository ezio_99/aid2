{-# OPTIONS_GHC -Wall #-}

module Gen where

import Test.QuickCheck
import AID2.Data

genTV :: IO GeneralType
genTV = do
  n <- generate $ elements ['a'..'z']
  i <- generate arbitrary :: IO Int
  return $ TypeVar Nothing $ n : (show . abs) i

genConcrete :: IO GeneralType
genConcrete = do
  c <- generate arbitrary :: IO Type''
  return $ ConcreteType c

genWithoutTV :: IO GeneralType
genWithoutTV = do
    c <- generate arbitrary :: IO GeneralType
    case c of
      TypeVar _ _ -> genWithoutTV
      _ -> return c

genGT :: IO GeneralType
genGT = generate arbitrary

genList :: IO GeneralType
genList = ListType' <$> genGT

genGTList :: IO [GeneralType]
genGTList = generate arbitrary

_genNotEqualTo :: (Monad m, Eq b) => m b -> b -> m b
_genNotEqualTo genFN x = do
  y <- genFN
  if x == y then _genNotEqualTo genFN x else return y

genGTNotEqualTo :: GeneralType -> IO GeneralType
genGTNotEqualTo = _genNotEqualTo genGT

genTVNotEqualTo :: GeneralType -> IO GeneralType
genTVNotEqualTo = _genNotEqualTo genTV
