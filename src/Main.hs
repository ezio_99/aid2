{-# OPTIONS_GHC -Wall #-}

module Main where

import AID2.Data
import AID2.Errors
import AID2.Eval.Eval
import AID2.Helpers
import AID2.Infer.Context
import AID2.Infer.Data
import AID2.Infer.Infer
import Control.Monad.Except
import Control.Monad.State
import Data.List (intercalate)
import Data.Maybe
import Normal.Abs
import Normal.Par
import Normal.Print
import qualified System.Directory.Internal.Prelude as SE
import System.Environment.MrEnv
import System.Exit
import System.IO

type Load a = StateT ST IO a

throwErrors :: FilePath -> [Error] -> IO ()
throwErrors fname errs = do
  putStrLn $ intercalate "\n" $ fmap (printError fname) errs
  exitFailure

main :: IO ()
main = do
  args <- SE.getArgs
  case listToMaybe args of
    Nothing -> putStrLn "Usage: aid2 filename"
    Just fname -> runFile fname

runFile :: String -> IO ()
runFile fname = do
  program <- parseSource fname
  case program of
    Right prog -> do
      res <- runProgram startState fname prog
      case res of
        Left errs -> do
          putStrLn $ intercalate "\n" $ fmap (printError fname) errs
          exitFailure
        Right (ed, _) -> do
          debug <- envAsBool "AID2_DEBUG" False
          if debug
            then runProg fname ed
            else evalProg fname ed
    Left err -> do
      putStrLn $ printError fname err
      exitFailure

runProgram :: ST -> FilePath -> Program -> IO (Either [Error] ([Expr], ST))
runProgram st _ (Program _ imports exprs) = do
  (importedExprs, newST) <- liftIO $ runStateT (mapM importFile imports) st
  let res = typecheck newST exprs
  case res of
    Left errs -> return $ Left errs
    Right (typecheckedExprs, newST') ->
      return $ Right (concat importedExprs ++ typecheckedExprs, newST')

parseSource :: FilePath -> IO (Either Error Program)
parseSource fname = do
  handle <- openFile fname ReadMode
  source <- hGetContents handle
  case pProgram (myLexer source) of
    Left err -> do
      hClose handle
      return $ Left $ Error Nothing err
    Right prog -> do
      hClose handle
      return $ Right prog

importFile :: Import -> Load [Expr]
importFile (Import _ fname) = do
  program <- liftIO $ parseSource fname
  case program of
    Left err -> do
      liftIO $ throwErrors fname [err]
      return []
    Right prog -> do
      st <- get
      res <- liftIO $ runProgram st fname prog
      case res of
        Left errs -> do
          liftIO $ throwErrors fname errs
          return []
        Right (exprData, newST) -> do
          put newST
          return exprData

printResult :: Expr -> IO ()
printResult expr = case expr of
  ConstUnit _ -> return ()
  _ -> putStrLn $ exprToString expr

runProg :: FilePath -> [Expr] -> IO ()
runProg fname es = do
  ios <- evalSeq es
  case ios of
    Left err -> liftIO $ throwErrors fname [err]
    Right exs -> mapM_ (putStrLn . printTree) exs

evalProg :: FilePath -> [Expr] -> IO ()
evalProg fname es = do
  ios <- evalSeq es
  case ios of
    Left err -> liftIO $ throwErrors fname [err]
    Right _ -> return ()
