{-# OPTIONS_GHC -Wall #-}

module AID2.Errors where

import AID2.Data
import AID2.Helpers
import System.Exit
import Text.Printf

printError :: FilePath -> Error -> String
printError f err = case err of
  UndefinedStruct pos s ->
    printf "UndefinedStruct: Struct %s is undefined%s" s $ p pos
  NonStruct pos ex ->
    printf "NonStruct: %s is not struct%s" (exprToString ex) $ p pos
  NonStruct' pos ->
    printf "NonStruct: Not struct%s" $ p pos
  ArityMismatch pos ->
    printf "ArityMismatch: Arity mismatch%s" $ p pos
  NonFunction pos ex ->
    printf "NonFunction: %s is not function%s" (exprToString ex) $ p pos
  NonFunction' pos ->
    printf "NonFunction: Non function%s" $ p pos
  CannotInfer pos gt ->
    printf "CannotInfer: Cannot infer type of %s%s" (generalTypeToName gt) $ p pos
  UndefinedFieldAccess pos ex s ->
    printf
      "UndefinedFieldAccess: Cannot access to undefined field %s of %s%s"
      s
      (exprToString ex)
      $ p pos
  CannotRedefine pos s ->
    printf "CannotRedefine: Identifier %s already taken%s" s $ p pos
  TypeDoesNotExist pos ty ->
    printf "TypeDoesNotExist: Type %s does not exist%s" (typeToName ty) $ p pos
  ContainsConflictingNamesError pos s ->
    printf "ContainsConflictingNamesError: Conflicting names for %s%s" s $ p pos
  ConflictingArgumentNamesError pos ->
    printf "ConflictingArgumentNamesError: Conflicting arguments%s" $ p pos
  ContainsUndefinedTypesError pos s ->
    printf "ContainsUndefinedTypesError: Undefined types for %s%s" s $ p pos
  OccursCheck pos gt gt' ->
    printf
      "OccursCheck: Cannot construct the infinite type: %s = %s%s"
      (generalTypeToName gt)
      (generalTypeToName gt')
      $ p pos
  EmptyListError pos ex ->
    printf "EmptyListError: Empty list in %s%s" (exprToString ex) $ p pos
  NonListType pos gt ->
    printf "NonListType: Expected List type, but got %s%s" (generalTypeToName gt) $ p pos
  EmptyBlock pos ->
    printf "EmptyBlock: Block should not be empty%s" $ p pos
  NonListExpr pos ex ->
    printf "NonListExpr: Expected List, but got %s%s" (exprToString ex) $ p pos
  TypeMismatchMany pos gtss gts ->
    printf
      "TypeMismatchMany: Expected one of %s, but got %s%s"
      (show (fmap (fmap generalTypeToName) gtss))
      (show (fmap generalTypeToName gts))
      $ p pos
  Error pos s ->
    printf "Error: %s%s" s $ p pos
  Undefined pos name ->
    printf "Undefined: %s is undefined%s" name $ p pos
  TypeError pos e ->
    printf "TypeError: Expected %s%s" (generalTypeToName e) (p pos)
  TypeErrorMany pos es ->
    printf "TypeError: Expected %s%s" (show $ generalTypeToName <$> es) (p pos)
  TypeMismatch pos e a ->
    printf
      "TypeMismatch: Expected %s, but got %s%s"
      (generalTypeToName e)
      (generalTypeToName a)
      $ p pos
  ZeroDivisionError pos e ->
    printf "ZeroDivisionError: %s%s" (exprToString e) (p pos)
  where
    p pos = printf "\n    at %s" f <> pp pos
    pp pos = case pos of
      Nothing -> ""
      Just (line, col) -> printf ":%d:%d" line col

runtimeError :: FilePath -> Error -> IO a
runtimeError f err = do
  putStrLn $ printError f err
  exitFailure
