{-# OPTIONS_GHC -Wall #-}

module AID2.Eval.Context where
import AID2.Eval.Data
import AID2.Scope

predef :: Context
predef = emptyScope
