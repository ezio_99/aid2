{-# OPTIONS_GHC -Wall #-}

module AID2.Eval.Data where

import Normal.Abs
import AID2.Scope
import Control.Monad.Except
import Control.Monad.State
import AID2.Data
newtype ContextValue = Expression Expr deriving (Show)

type Context = Scope ContextValue

type Eval a = ExceptT Error (StateT [Context] IO) a
