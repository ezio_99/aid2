{-# OPTIONS_GHC -Wall #-}

module AID2.Eval.Eval where

import AID2.Data
import AID2.Eval.Context
import AID2.Eval.Data
import AID2.Helpers
import AID2.Scope
import Control.Monad.Except
import Control.Monad.State.Lazy
import Data.List (isInfixOf, isPrefixOf)
import qualified Data.Text as Text
import Normal.Abs
import System.IO

evalSeq :: [Expr] -> IO (Either Error [Expr])
evalSeq es = flip evalStateT [predef] . runExceptT $ mapM eval es

evalBlock :: [Expr] -> Eval [Expr]
evalBlock = mapM eval

eval :: Expr -> Eval Expr
eval e = do
  scopes <- get
  case e of
    ConstTrue _ -> return $ ConstTrue Nothing
    ConstFalse _ -> return $ ConstFalse Nothing
    Integer _ int -> return $ Integer Nothing int
    Double _ double -> return $ Double Nothing double
    NegativeInteger _ int -> return $ Integer Nothing (-int)
    NegativeDouble _ double -> return $ Double Nothing (-double)
    String _ str -> return $ String Nothing str
    ConstUnit _ -> return $ ConstUnit Nothing
    Var pos (Ident name) -> case getFromContextOrError pos name scopes of
      Left err -> throwError err -- TODO: should we check this after typecheck ???
      Right (Expression ex) -> return ex
    Binding _ (Ident name) right -> do
      rs <- eval right
      modify (addToScopes name $ Expression rs)
      return $ ConstUnit Nothing
    StructDef _ ident@(Ident name) fields -> do
      modify (addToScopes name $ Expression $ StructDef Nothing ident fields)
      return $ ConstUnit Nothing
    Builtin _ bc -> evalBuiltin bc
    IOBuiltin _ iobc -> evalIOBuiltin iobc
    Dot pos expr1 fieldName -> do
      r <- eval expr1
      case r of
        CreateStruct _ (Ident structName) fields -> case getFromContextOrError pos structName scopes of
          Left er -> throwError er
          Right cv -> case cv of
            Expression (StructDef _ _ sfs) ->
              return $ commaExprToExpr fields !! getFieldIndex sfs fieldName
            _ -> throwError $ UndefinedStruct pos structName
        _ -> throwError $ NonStruct pos expr1
    CreateStruct _ ident fields -> do
      fieldValues <- evalBlock $ commaExprToExpr fields
      return $ CreateStruct Nothing ident $ map (CommaExpr Nothing) fieldValues
    List _ es -> do
      res <- evalBlock es
      return $ List Nothing res
    FuncCall pos (Ident fname) commedArgs -> case getFromContextOrError pos fname scopes of
      Right (Expression ex) -> funcCall ex commedArgs
      Left err -> throwError err
    DottedFuncCall _ dottedAccess args -> do
      fn <- eval dottedAccess
      funcCall fn args
    ta@(TypeAlias _ (Ident n) _) -> do
      modify (addToScopes n $ Expression ta)
      return ta
    If _ cond thenBlock elseBlock -> do
      rs <- eval cond
      let branch = if rs == ConstTrue Nothing then thenBlock else elseBlock
      rs' <- evalBlock (blockOfCodeToExprs branch)
      return $ last rs'
    TypedFuncDef _ (Ident n) args _ body -> do
      modify (addToScopes n (Expression $ LambdaDef Nothing args body))
      return $ ConstUnit Nothing
    TypelessFuncDef _ (Ident n) args body -> do
      modify (addToScopes n (Expression $ LambdaDef Nothing args body))
      return $ ConstUnit Nothing
    la@LambdaDef {} -> return la
    LambdaCall _ args body callArgs -> funcCall (LambdaDef Nothing args (Block Nothing body)) callArgs

getFieldIndex :: [StructField] -> Ident -> Int
getFieldIndex sfs ident = _getFieldIndex sfs ident 0
  where
    _getFieldIndex [] _ _ = -1
    _getFieldIndex (StructField _ (Typing _ (Ident name) _) : xs) ident_@(Ident fieldName) idx =
      if fieldName == name
        then idx
        else _getFieldIndex xs ident_ (idx + 1)

funcCall :: Expr -> [CommaExpr] -> Eval Expr
funcCall func commedArgs = do
  scopes <- get
  case func of
    LambdaDef _ las body -> do
      computedArgs <- evalBlock args
      let newscopes = addLocalsToContext las computedArgs scopes
      put newscopes
      fnres <- evalBlock (blockOfCodeToExprs body)
      put scopes
      return $ last fnres
    _ -> throwError $ NonFunction (hasPosition func) func
  where
    args = commaExprToExpr commedArgs

addLocalsToContext :: [FuncArg] -> [Expr] -> [Context] -> [Context]
addLocalsToContext las args scopes = foldl add emptyScope (zip (fmap argNameFromFuncArg las) args) : scopes
  where
    add acc (n, e) = addToScope n (Expression e) acc

----------------------------------------------------------------------------------
-- Builtins
----------------------------------------------------------------------------------

bcHead :: Expr -> Eval Expr
bcHead ex = do
  res <- eval ex
  case res of
    List _ exs -> case exs of
      [] -> throwError $ EmptyListError (hasPosition ex) ex
      ex' : _ -> return ex'
    _ -> throwError $ NonListExpr (hasPosition ex) ex

bcTail :: Expr -> Eval Expr
bcTail ex = do
  res <- eval ex
  case res of
    List _ exs -> case exs of
      [] -> throwError $ EmptyListError (hasPosition ex) ex
      _ : exs' -> return $ List (hasPosition ex) exs'
    _ -> throwError $ NonListExpr (hasPosition ex) ex

bcCons :: Expr -> Expr -> Eval Expr
bcCons ex exs = do
  ex' <- eval ex
  exs' <- eval exs
  case exs' of
    List _ exss -> return $ List Nothing $ ex' : exss
    _ -> throwError $ NonListExpr (hasPosition exs) exs

bcPlus :: Expr -> Expr -> Eval Expr
bcPlus ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  case ex1' of
    Integer _ i1 -> case ex2' of
      Integer _ i2 -> return $ Integer Nothing $ i1 + i2
      Double _ d2 -> return $ Double Nothing $ fromIntegral i1 + d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    Double _ d1 -> case ex2' of
      Integer _ i2 -> return $ Double Nothing $ d1 + fromIntegral i2
      Double _ d2 -> return $ Double Nothing $ d1 + d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    _ -> throwError $ TypeErrorMany (hasPosition ex1) numericTypes

bcMinus :: Expr -> Expr -> Eval Expr
bcMinus ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  case ex1' of
    Integer _ i1 -> case ex2' of
      Integer _ i2 -> return $ Integer Nothing $ i1 - i2
      Double _ d2 -> return $ Double Nothing $ fromIntegral i1 - d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    Double _ d1 -> case ex2' of
      Integer _ i2 -> return $ Double Nothing $ d1 - fromIntegral i2
      Double _ d2 -> return $ Double Nothing $ d1 - d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    _ -> throwError $ TypeErrorMany (hasPosition ex1) numericTypes

bcTimes :: Expr -> Expr -> Eval Expr
bcTimes ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  case ex1' of
    Integer _ i1 -> case ex2' of
      Integer _ i2 -> return $ Integer Nothing $ i1 * i2
      Double _ d2 -> return $ Double Nothing $ fromIntegral i1 * d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    Double _ d1 -> case ex2' of
      Integer _ i2 -> return $ Double Nothing $ d1 * fromIntegral i2
      Double _ d2 -> return $ Double Nothing $ d1 * d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    _ -> throwError $ TypeErrorMany (hasPosition ex1) numericTypes

bcDivide :: Expr -> Expr -> Eval Expr
bcDivide ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  case ex1' of
    Integer _ i1 -> case ex2' of
      Integer _ i2 ->
        if i2 == 0
          then throwError $ ZeroDivisionError (hasPosition ex2) ex2
          else return $ Integer Nothing $ div i1 i2
      Double _ d2 ->
        if d2 == 0
          then throwError $ ZeroDivisionError (hasPosition ex2) ex2
          else return $ Double Nothing $ fromIntegral i1 / d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes
    Double _ d1 -> case ex2' of
      Integer _ i2 ->
        if i2 == 0
          then throwError $ ZeroDivisionError (hasPosition ex2) ex2
          else return $ Double Nothing $ d1 / fromIntegral i2
      Double _ d2 ->
        if d2 == 0
          then throwError $ ZeroDivisionError (hasPosition ex2) ex2
          else return $ Double Nothing $ d1 / d2
      _ -> throwError $ TypeErrorMany (hasPosition ex2) numericTypes  
    _ -> throwError $ TypeErrorMany (hasPosition ex1) numericTypes

bcEqual :: Expr -> Expr -> Eval Expr
bcEqual ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  return $ boolToExpr $ ex1' == ex2'

bcLess :: Expr -> Expr -> Eval Expr
bcLess ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  return $ boolToExpr $ ex1' < ex2'

bcGreater :: Expr -> Expr -> Eval Expr
bcGreater ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  return $ boolToExpr $ ex1' > ex2'

bcNot :: Expr -> Eval Expr
bcNot ex = do
  ex' <- eval ex
  return $ boolToExpr $ ex' /= true

bcAnd :: Expr -> Expr -> Eval Expr
bcAnd ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  return $ boolToExpr $ ex1' == true && ex2' == true

bcOr :: Expr -> Expr -> Eval Expr
bcOr ex1 ex2 = do
  ex1' <- eval ex1
  ex2' <- eval ex2
  return $ boolToExpr $ ex1' == true || ex2' == true

bcIntToDouble :: Expr -> Eval Expr
bcIntToDouble ex = do
  ex' <- eval ex
  case ex' of
    Integer _ i -> return $ Double Nothing $ fromIntegral i
    _ -> throwError $ TypeError (hasPosition ex) intType

bcIntToString :: Expr -> Eval Expr
bcIntToString ex = do
  ex' <- eval ex
  case ex' of
    Integer _ i -> return $ String Nothing $ show i
    _ -> throwError $ TypeError (hasPosition ex) intType

bcDoubleToString :: Expr -> Eval Expr
bcDoubleToString ex = do
  ex' <- eval ex
  case ex' of
    Double _ d -> return $ String Nothing $ show d
    _ -> throwError $ TypeError (hasPosition ex) doubleType

bcPrint :: Expr -> Eval Expr
bcPrint ex = do
  res <- eval ex
  liftIO $ putStr $ exprToString res
  liftIO $ hFlush stdout
  return unit

bcGetLine :: Eval Expr
bcGetLine = liftIO $ String Nothing <$> getLine

bcStringContains :: Expr -> Expr -> Eval Expr
bcStringContains str substr = do
  str' <- eval str
  substr' <- eval substr
  case str' of
    String _ s -> case substr' of
      String _ ss -> return $ boolToExpr $ ss `isInfixOf` s
      _ -> throwError $ TypeError (hasPosition str) stringType
    _ -> throwError $ TypeError (hasPosition substr) stringType

bcTrim :: Expr -> Eval Expr
bcTrim str = do
  str' <- eval str
  case str' of
    String _ s -> return $ String Nothing $ Text.unpack $ Text.strip $ Text.pack s
    _ -> throwError $ TypeError (hasPosition str) stringType

bcLower :: Expr -> Eval Expr
bcLower str = do
  str' <- eval str
  case str' of
    String _ s -> return $ String Nothing $ Text.unpack $ Text.toLower $ Text.pack s
    _ -> throwError $ TypeError (hasPosition str) stringType

bcStringStartsWith :: Expr -> Expr -> Eval Expr
bcStringStartsWith str substr = do
  str' <- eval str
  substr' <- eval substr
  case str' of
    String _ s -> case substr' of
      String _ ss -> return $ boolToExpr $ ss `isPrefixOf` s
      _ -> throwError $ TypeError (hasPosition substr) stringType
    _ -> throwError $ TypeError (hasPosition str) stringType

bcIsListEmpty :: Expr -> Eval Expr
bcIsListEmpty ex = do
  ex' <- eval ex
  case ex' of
    List _ [] -> return true
    List _ _ -> return false
    _ -> throwError $ NonListExpr (hasPosition ex) ex

bcDropFromStringStart :: Expr -> Expr -> Eval Expr
bcDropFromStringStart str n = do
  str' <- eval str
  n' <- eval n
  case str' of
    String _ s -> case n' of
      Integer _ num -> return $ String Nothing $ drop (fromIntegral num) s
      _ -> throwError $ TypeError (hasPosition n) stringType
    _ -> throwError $ TypeError (hasPosition str) intType

evalBuiltin :: BuiltinCall -> Eval Expr
evalBuiltin bc = case bc of
  Head _ ex -> bcHead ex
  Tail _ ex -> bcTail ex
  Cons _ ex exs -> bcCons ex exs
  Plus _ ex1 ex2 -> bcPlus ex1 ex2
  Minus _ ex1 ex2 -> bcMinus ex1 ex2
  Times _ ex1 ex2 -> bcTimes ex1 ex2
  Divide _ ex1 ex2 -> bcDivide ex1 ex2
  Equal _ ex1 ex2 -> bcEqual ex1 ex2
  Less _ ex1 ex2 -> bcLess ex1 ex2
  Greater _ ex1 ex2 -> bcGreater ex1 ex2
  Not _ ex -> bcNot ex
  And _ ex1 ex2 -> bcAnd ex1 ex2
  Or _ ex1 ex2 -> bcAnd ex1 ex2
  IntToDouble _ i -> bcIntToDouble i
  IntToString _ i -> bcIntToString i
  DoubleToString _ d -> bcDoubleToString d
  EmptyList _ _ -> return $ List Nothing []
  StringContains _ str substr -> bcStringContains str substr
  Trim _ str -> bcTrim str
  Lower _ str -> bcLower str
  StringStartsWith _ str substr -> bcStringStartsWith str substr
  IsListEmpty _ ex -> bcIsListEmpty ex
  DropFromStringStart _ str n -> bcDropFromStringStart str n

evalIOBuiltin :: IOBuiltinCall -> Eval Expr
evalIOBuiltin iobc = case iobc of
  Print _ ex -> bcPrint ex
  PrintLn pos ex -> do
    _ <- bcPrint ex
    liftIO $ putStr "\n"
    return $ ConstUnit pos
  GetLine _ -> bcGetLine
