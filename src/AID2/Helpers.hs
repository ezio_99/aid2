{-# OPTIONS_GHC -Wall #-}

module AID2.Helpers where

import Data.Char (isUpper)
import Data.List (intercalate)
import qualified Data.Set as Set
import Normal.Abs
import Normal.Print (printTree)
import AID2.Data

hasDuplicates :: (Ord a) => [a] -> Bool
hasDuplicates list = length list /= length set
  where
    set = Set.fromList list

typeFromFuncArg :: FuncArg -> Maybe Type
typeFromFuncArg (Typed _ (Typing _ _ t)) = Just t
typeFromFuncArg _ = Nothing

typingsFromStructFields :: [StructField] -> [Typing]
typingsFromStructFields = fmap (\(StructField _ t) -> t)

typesFromTypings :: [Typing] -> [Type]
typesFromTypings = fmap (\(Typing _ _ t) -> t)

typesFromStructFields :: [StructField] -> [Type]
typesFromStructFields = typesFromTypings . typingsFromStructFields

commaExprToExpr :: [CommaExpr] -> [Expr]
commaExprToExpr = fmap (\(CommaExpr _ e) -> e)

nameFromFuncArg :: FuncArg -> Name
nameFromFuncArg (Typed _ (Typing _ (Ident n) _)) = n
nameFromFuncArg (Typeless _ (TypelessArg _ (Ident n))) = n

argNameFromFuncArg :: FuncArg -> String
argNameFromFuncArg la = case la of
  Typed _ (Typing _ (Ident n) _) -> n
  Typeless _ (TypelessArg _ (Ident n)) -> n

boolToExpr :: Bool -> Expr
boolToExpr e = if e then ConstTrue Nothing else ConstFalse Nothing

isBlockOfCodeEmpty :: BlockOfCode -> Bool
isBlockOfCodeEmpty boc = case boc of
  Block _ exprs -> null exprs
  Single _ _ -> False

blockOfCodeToExprs :: BlockOfCode -> [Expr]
blockOfCodeToExprs boc = case boc of
  Block _ exprs -> exprs
  Single _ expr -> [expr]

typeToName :: Type -> Name
typeToName ty = case ty of
  FuncType _ typings t2 -> "<" <> intercalate ", " (fmap typeToName typings) <> "> -> " <> typeToName t2
  ListType _ t -> "[" <> typeToName t <> "]"
  StringType _ -> "String"
  BoolType _ -> "Bool"
  DoubleType _ -> "Double"
  UnitType _ -> "Unit"
  IntType _ -> "Int"
  DefinedType _ (Ident n) -> n

exprToString :: Expr -> String
exprToString expr = case expr of
  Var _ (Ident n) -> n
  Dot _ ex (Ident n) -> exprToString ex <> "." <> n
  List _ exs -> "[" <> intercalate ", " (fmap exprToString exs) <> "]"
  FuncCall _ (Ident n) ces -> n <> "(" <> intercalate ", " (fmap exprToString (commaExprToExpr ces)) <> ")"
  CreateStruct _ (Ident n) ces -> new <> params <> close
    where
      new = "new " <> n <> "(\n"
      params = intercalate ",\n\t" (fmap exprToString (commaExprToExpr ces))
      close = "\n)"
  Integer _ n -> show n
  Double _ x -> show x
  NegativeInteger _ n -> show n
  NegativeDouble _ x -> show x
  String _ s -> s
  ConstTrue _ -> "True"
  ConstFalse _ -> "False"
  ConstUnit _ -> "unit"
  If _ ex boc boc' -> "if " <> cond <> "then {\n" <> then_ <> "\n} else" <> else_ <> "\n}"
    where
      cond = exprToString ex
      then_ = intercalate ";\n\t" (fmap exprToString (blockOfCodeToExprs boc))
      else_ = intercalate ";\n\t" (fmap exprToString (blockOfCodeToExprs boc'))
  StructDef _ _ sfs -> "struct {\n" <> fields <> "\n}"
    where
      fs = fmap (\(StructField _ (Typing _ (Ident n) t)) -> n <> ": " <> typeToName t) sfs
      fields = intercalate ";\n\t" fs
  TypeAlias _ (Ident n) ty -> "type " <> n <> " = " <> typeToName ty
  Binding _ (Ident n) ex -> n <> " = " <> exprToString ex
  e -> printTree e

startsWithUpper :: [Char] -> Bool
startsWithUpper [] = False
startsWithUpper (x : _) = isUpper x

isExprConstTrue :: Expr -> Bool
isExprConstTrue (ConstTrue _) = True
isExprConstTrue _ = False

generalTypeToName :: GeneralType -> Name
generalTypeToName ty = case ty of
  FuncType' typings t2 -> "<" <> intercalate ", " (fmap generalTypeToName typings) <> "> -> " <> generalTypeToName t2
  ListType' t -> "[" <> generalTypeToName t <> "]"
  ConcreteType ty' -> case ty' of
    StringType' -> "String"
    BoolType' -> "Bool"
    DoubleType' -> "Double"
    UnitType' -> "Unit"
    IntType' -> "Int"
  TypeVar _ tv -> tv
  DefinedType' n -> n
