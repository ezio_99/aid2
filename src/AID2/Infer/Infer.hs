{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.Infer where

import AID2.Data
import AID2.Infer.Data
import AID2.Infer.TypeOf
import AID2.Infer.Unify
import Control.Monad.Except
import Control.Monad.State
import qualified Data.Set as Set
import Normal.Abs

typecheck :: ST -> [Expr] -> Either [Error] ([Expr], ST)
typecheck = infer

infer :: ST -> [Expr] -> Either [Error] ([Expr], ST)
infer startST exprs = do
  let ( res,
        st@ST
          { getConstraints = cs,
            getUsedTypeVars = usedTypeVars,
            getConstrainedTypeVars = constrainedTypeVars
          }
        ) = flip runState startST . runExceptT $ mapM_ typeOf exprs
  case res of
    Left err -> Left [err]
    Right _ -> do
      let diff = Set.difference usedTypeVars constrainedTypeVars
      if Set.null diff
        then do
          let constraints = reverse cs
          case runExcept (unify constraints) of
            Left err -> Left [err]
            Right _ -> Right (exprs, st {getConstraints = [], getConstrainedTypeVars = Set.empty, getUsedTypeVars = Set.empty})
        else Left $ f <$> Set.toList diff
      where
        f gt = case gt of
          TypeVar pos _ -> CannotInfer pos gt
          _ -> CannotInfer Nothing gt