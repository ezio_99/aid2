{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.Unify where

import AID2.Data
import AID2.Infer.Data
import AID2.Infer.Helpers
import Control.Monad.Except
import Normal.Abs (BNFC'Position)

newtype Sub = Sub GeneralType

newtype Target = Target GeneralType

replace :: Name -> Sub -> Target -> Except Error GeneralType
replace tv s@(Sub sub) (Target target) = case target of
  TypeVar _ tv' -> do
    let x = if tv == tv' then sub else target
    return x
  FuncType' args retType -> do
    args' <- mapM (replace tv s . Target) args
    retType' <- replace tv s (Target retType)
    return $ FuncType' args' retType'
  ListType' items -> do
    items' <- replace tv s (Target items)
    return $ ListType' items'
  _ -> return target

subst :: BNFC'Position -> GeneralType -> GeneralType -> [Constraint] -> Except Error [Constraint]
subst pos t1 t2 cons
  | t1 == t2 = return cons
  | otherwise = subst' t1 t2
  where
    subst' (ConcreteType _) (TypeVar _ tv) = tvAndAny tv (Sub t1) (Target t2) cons
    subst' (TypeVar _ tv) _ = tvAndAny tv (Sub t2) (Target t1) cons
    subst' _ (TypeVar _ tv) = tvAndAny tv (Sub t1) (Target t2) cons
    subst' (FuncType' args1 ret1) (FuncType' args2 ret2) = return $ funcAndFunc args1 ret1 args2 ret2
    subst' (ListType' i) (ListType' i') = return $ listAndList i i'
    subst' _ _ = throwError $ cannotSubstituteError pos t1 t2

    listAndList t1' t2' = Constraint pos t1' t2' : cons
    funcAndFunc args1 ret1 args2 ret2 = fmap (uncurry $ Constraint pos) (zip args1 args2) ++ [Constraint pos ret1 ret2] ++ cons
    tvAndAny tv s t cons' = do
      r <- replace tv s t
      occursCheck pos (Sub $ TypeVar pos tv) (Target r)
      replaceAll tv s cons'

replaceAll :: Name -> Sub -> [Constraint] -> Except Error [Constraint]
replaceAll tv s = mapM f
  where
    f (Constraint pos t1 t2) = do
      t1' <- replace tv s (Target t1)
      occursCheck pos (Sub $ TypeVar pos tv) (Target t1')
      t2' <- replace tv s (Target t2)
      occursCheck pos (Sub $ TypeVar pos tv) (Target t2')
      return $ Constraint pos t1' t2'

unify :: [Constraint] -> Except Error ()
unify [] = return ()
unify (Constraint pos t1 t2 : cons) = do
  newCons <- subst pos t1 t2 cons
  unify newCons

occursCheck :: BNFC'Position -> Sub -> Target -> Except Error ()
occursCheck pos (Sub tv) (Target target) = case target of
  TypeVar _ _ -> return ()
  _ -> when (tv `elem` typeVarsFromGeneralType target) cannotConstruncInfiniteType
  where
    cannotConstruncInfiniteType :: Except Error ()
    cannotConstruncInfiniteType = throwError $ OccursCheck pos target tv
