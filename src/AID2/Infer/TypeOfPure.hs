{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.TypeOfPure where

import AID2.Data
import AID2.Infer.Context
import AID2.Infer.Data
import AID2.Infer.Helpers
import Control.Monad.Except
import Control.Monad.State
import Normal.Abs

typeOfListPure :: ST -> BNFC'Position -> [(GeneralType, BNFC'Position)] -> (Either Error GeneralType, ST)
typeOfListPure st pos lstTypes = runState (runExceptT logic) st
  where
    logic :: ExceptT Error (State ST) GeneralType
    logic = do
      let (concretes, tvs) = splitGeneralTypes lstTypes
      case findNonMatchingType concretes of
        Nothing -> collectListConstraints pos concretes tvs
        Just (e, (a, pos')) -> throwError $ TypeMismatch pos' e a

    findNonMatchingType :: [(GeneralType, BNFC'Position)] -> Maybe (GeneralType, (GeneralType, BNFC'Position))
    findNonMatchingType [] = Nothing
    findNonMatchingType [_] = Nothing
    findNonMatchingType ((gt1, _) : (gt2, pos') : xs) =
      if gt1 == gt2
        then findNonMatchingType $ (gt2, pos') : xs
        else Just (gt1, (gt2, pos'))

    -- return (ConcreteTypesList, TypeVarsList)
    splitGeneralTypes = foldl s ([], [])
      where
        s (cs, tvs) i@(gt, _) =
          if isContainsTypeVars gt
            then (cs, tvs ++ [i])
            else (cs ++ [i], tvs)

    collect :: (GeneralType, BNFC'Position) -> (GeneralType, BNFC'Position) -> ExceptT Error (State ST) GeneralType
    collect (le, _) (ri, pos') = do
      modify (addConstraint $ Constraint pos' le ri)
      return le

    collectListConstraints :: BNFC'Position -> [(GeneralType, BNFC'Position)] -> [(GeneralType, BNFC'Position)] -> ExceptT Error (State ST) GeneralType
    collectListConstraints pos' concretes tvs = case concretes of
      [] -> case tvs of
        [] -> do
          ftv <- takeFreshTV pos'
          return $ ListType' ftv
        i@(gt, _) : gts -> do
          mapM_ (collect i) gts
          return $ ListType' gt
      i@(gt, _) : _ -> case tvs of
        [] -> return $ ListType' gt
        tvs' -> do
          mapM_ (collect i) tvs'
          return $ ListType' gt
