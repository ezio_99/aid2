{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.Helpers where

import AID2.Data
import AID2.Infer.Data
import AID2.Scope
import Normal.Abs

typesDontMatchError :: BNFC'Position -> GeneralType -> GeneralType -> Error
typesDontMatchError = TypeMismatch

cannotSubstituteError :: BNFC'Position -> GeneralType -> GeneralType -> Error
cannotSubstituteError = TypeMismatch

typesFromTypings' :: [Typing''] -> [GeneralType]
typesFromTypings' = fmap (\(Typing' _ ty) -> ty)

isEqualList :: (Eq a) => [a] -> Bool
isEqualList xs = case xs of
  [] -> True
  a : as -> and $ fmap (a ==) as

generalTypeFromType :: Type -> GeneralType
generalTypeFromType t = case t of
  FuncType _ tys ty -> FuncType' (fmap generalTypeFromType tys) (generalTypeFromType ty)
  ListType _ ty -> ListType' (generalTypeFromType ty)
  StringType _ -> ConcreteType StringType'
  BoolType _ -> ConcreteType BoolType'
  UnitType _ -> ConcreteType UnitType'
  IntType _ -> ConcreteType IntType'
  DoubleType _ -> ConcreteType DoubleType'
  DefinedType _ (Ident n) -> DefinedType' n

isTypeExists :: [Context] -> Type -> Bool
isTypeExists scopes ty = case ty of
  FuncType _ ts t -> and $ isTypeExists scopes t : fmap (isTypeExists scopes) ts
  ListType _ t -> isTypeExists scopes t
  DefinedType _ (Ident ident) -> inScopes ident scopes
  _ -> True

typeOfContextValue :: ContextValue -> GeneralType
typeOfContextValue cv = case cv of
  Function args retType -> FuncType' (typesFromTypings' args) retType
  Struct n _ -> DefinedType' n
  Variable _ gt -> gt
  Alias _ gt -> gt
  PredefinedType gt -> gt

isContainsTypeVars :: GeneralType -> Bool
isContainsTypeVars gt = case gt of
  TypeVar _ _ -> True
  FuncType' gts gt' -> isContainsTypeVars gt' || or (fmap isContainsTypeVars gts)
  ListType' gt' -> isContainsTypeVars gt'
  _ -> False

typeVarsFromConstraint :: Constraint -> [GeneralType]
typeVarsFromConstraint (Constraint _ t1 t2) = x ++ y
  where
    x = typeVarsFromGeneralType t1
    y = typeVarsFromGeneralType t2

typeVarsFromGeneralType :: GeneralType -> [GeneralType]
typeVarsFromGeneralType gt = case gt of
  TypeVar _ _ -> [gt]
  ListType' items -> typeVarsFromGeneralType items
  FuncType' args retType -> concatMap typeVarsFromGeneralType args ++ typeVarsFromGeneralType retType
  _ -> []
