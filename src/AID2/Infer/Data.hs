{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE DeriveGeneric #-}

module AID2.Infer.Data where

import AID2.Scope
import Control.Monad.Except
import Control.Monad.State
import Normal.Abs
import Test.QuickCheck
import GHC.Generics
import Data.Set
import AID2.Data

data ContextValue
  = Function [Typing''] GeneralType
  | Struct Name [StructField]
  | Variable Name GeneralType
  | Alias Name GeneralType
  | PredefinedType GeneralType
  deriving (Show, Eq)

data Typing'' = Typing' Name GeneralType
  deriving (Show, Eq, Generic)

type Context = Scope ContextValue

data Constraint = Constraint BNFC'Position GeneralType GeneralType
  deriving (Show)

instance Eq Constraint where
  (==) (Constraint _ t1 t2) (Constraint _ t1' t2') =
    (t1 == t1' && t2 == t2') || (t1 == t2' && t2 == t1')

data ST = ST
  { getScopes :: [Context],
    getConstraints :: [Constraint],
    getFreshTypeVars :: [Name],
    getUsedTypeVars :: Set GeneralType,
    getConstrainedTypeVars :: Set GeneralType
  }
  deriving (Show, Eq)

type Collect a = ExceptT Error (State ST) a

newtype TypeVar = TV Name
  deriving (Show, Eq, Generic, Ord)

instance Arbitrary TypeVar where
  arbitrary = TV <$> arbitrary
