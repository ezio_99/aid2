{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.TypeOf where

import AID2.Data
import AID2.Helpers
import AID2.Infer.Context
import AID2.Infer.Data
import AID2.Infer.Helpers
import AID2.Infer.TypeOfPure
import AID2.Scope
import Control.Monad.Except
import Control.Monad.State
import Normal.Abs

-- Functions for extracting type from any expression

typeOf :: Expr -> Collect GeneralType
typeOf expr = case expr of
  Binding pos (Ident varName) expr1 -> typeOfBinding pos varName expr1
  CreateStruct pos (Ident ident) createArgs -> createStruct pos ident createArgs
  If _ cond thenBlock elseBlock -> typeOfIf cond thenBlock elseBlock
  List pos items -> typeOfList pos items
  DottedFuncCall pos dottedAccess args -> typeOfDottedFuncCall pos dottedAccess args
  FuncCall pos (Ident fname) args -> typeOfFuncCall_ pos fname args
  LambdaCall pos args body callArgs -> typeOfLambdaCall pos args body callArgs
  ------------------------------------------------------------------------------------
  Builtin _ bc -> typeOfBuiltin bc
  IOBuiltin _ iobc -> typeOfIOBuiltin iobc
  ------------------------------------------------------------------------------------
  TypedFuncDef pos (Ident fname) args retType body ->
    typeOfFuncDef pos (Just fname) args (blockOfCodeToExprs body) (generalTypeFromType retType)
  TypelessFuncDef pos (Ident fname) args body -> do
    retType <- takeFreshTV pos
    typeOfFuncDef pos (Just fname) args (blockOfCodeToExprs body) retType
  LambdaDef pos args body -> do
    retType <- takeFreshTV pos
    typeOfFuncDef pos Nothing args (blockOfCodeToExprs body) retType
  ------------------------------------------------------------------------------------
  TypeAlias pos (Ident name) t -> typeOfTypeAlias pos name t
  StructDef pos (Ident name) fields -> typeOfStructDef pos name fields
  Var pos (Ident name) -> typeOfVar pos name
  Dot _ expr1 (Ident fieldName) -> typeOfDottedAccess expr1 fieldName
  ------------------------------------------------------------------------------------
  Integer _ _ -> return $ ConcreteType IntType'
  Double _ _ -> return $ ConcreteType DoubleType'
  String _ _ -> return $ ConcreteType StringType'
  ConstTrue _ -> return $ ConcreteType BoolType'
  ConstFalse _ -> return $ ConcreteType BoolType'
  ConstUnit _ -> return $ ConcreteType UnitType'
  NegativeInteger _ _ -> return $ ConcreteType IntType'
  NegativeDouble _ _ -> return $ ConcreteType DoubleType'

typeOfBlock :: [Expr] -> Collect [GeneralType]
typeOfBlock = mapM typeOf

typeOfSeq :: [Expr] -> Collect GeneralType
typeOfSeq exprs = do
  res <- typeOfBlock exprs
  return $ last res

-- Functions for extracting type from specific expression

typeOfDottedAccess :: Expr -> Name -> Collect GeneralType
typeOfDottedAccess expr1 fieldName = do
  let pos = hasPosition expr1
  scopes <- gets getScopes
  expr1Type <- typeOf expr1
  case expr1Type of
    DefinedType' structName -> case getFromContextOrError pos structName scopes of
      Left er -> throwError er
      Right cv' -> case cv' of
        Struct _ fields -> case filter (\(StructField _ (Typing _ (Ident n) _)) -> n == fieldName) fields of
          [] -> throwError $ UndefinedFieldAccess pos expr1 fieldName
          StructField _ (Typing _ _ t) : _ -> return $ generalTypeFromType t
        _ -> throwError $ NonStruct pos expr1
    _ -> throwError $ NonStruct pos expr1

typeOfVar :: BNFC'Position -> Name -> Collect GeneralType
typeOfVar pos name = do
  scopes <- gets getScopes
  case getFromContextOrError pos name scopes of
    Left err -> throwError err
    Right cv -> return (typeOfContextValue cv)

typeOfStructDef :: BNFC'Position -> Name -> [StructField] -> Collect GeneralType
typeOfStructDef pos name fields = do
  scopes <- gets getScopes
  logic scopes
  where
    logic :: [Context] -> Collect GeneralType
    logic scopes
      | inScopes name scopes = throwError $ CannotRedefine pos name
      | hasDuplicates $ fmap (\(StructField _ (Typing _ n _)) -> n) fields =
        throwError $ ContainsConflictingNamesError pos name
      | not $ and $ isTypeExists scopes <$> typesFromStructFields fields =
        throwError $ ContainsUndefinedTypesError pos name
      | otherwise = do
        modify (addToContext name (Struct name fields))
        return $ ConcreteType UnitType'

typeOfTypeAlias :: BNFC'Position -> Name -> Type -> Collect GeneralType
typeOfTypeAlias pos name t = do
  scopes <- gets getScopes
  logic scopes
  where
    logic :: [Context] -> Collect GeneralType
    logic scopes
      | inScopes name scopes = throwError $ CannotRedefine pos name
      | isTypeExists scopes t = do
        ty <- normalizeGT (generalTypeFromType t)
        modify (addToContext name (Alias name ty))
        return $ ConcreteType UnitType'
      | otherwise = throwError $ TypeDoesNotExist pos t

typeOfLambdaCall :: BNFC'Position -> [FuncArg] -> [Expr] -> [CommaExpr] -> Collect GeneralType
typeOfLambdaCall pos args body callArgs = do
  retType <- takeFreshTV pos
  ty <- typeOfFuncDef pos Nothing args body retType
  case ty of
    FuncType' args' retType' -> do
      callTypes <- typeOfBlock $ commaExprToExpr callArgs
      typeOfFuncCall pos args' (zip callTypes $ fmap hasPosition callArgs) retType'
    _ -> throwError $ NonFunction' pos

typeOfFuncCall_ :: BNFC'Position -> Name -> [CommaExpr] -> Collect GeneralType
typeOfFuncCall_ pos fname args = do
  scopes <- gets getScopes
  case getFromContextOrError pos fname scopes of
    Left err -> throwError err
    Right cv' -> do
      argTypes <- typeOfBlock (commaExprToExpr args)
      case cv' of
        Function typings retType -> typeOfFuncCall pos (typesFromTypings' typings) (zip argTypes $ fmap hasPosition args) retType
        Variable _ ty -> case ty of
          FuncType' args_ retType_ -> typeOfFuncCall pos args_ (zip argTypes $ fmap hasPosition args) retType_
          tv@(TypeVar _ _) -> do
            ftv <- takeFreshTV pos
            let res = FuncType' argTypes ftv
            modify (addConstraint $ Constraint pos tv res)
            return res
          _ -> throwError $ NonFunction' pos
        _ -> throwError $ NonFunction' pos

typeOfDottedFuncCall :: BNFC'Position -> Expr -> [CommaExpr] -> Collect GeneralType
typeOfDottedFuncCall pos dottedAccess args = do
  ty <- typeOf dottedAccess
  case ty of
    FuncType' argTypes retType -> do
      computedArgTypes <- mapM typeOf (commaExprToExpr args)
      typeOfFuncCall pos argTypes (zip computedArgTypes $ fmap hasPosition args) retType
    _ -> throwError $ NonFunction' pos

typeOfList :: BNFC'Position -> [Expr] -> Collect GeneralType
typeOfList pos items = do
  lstTypes <- typeOfBlock items
  st <- get
  let (ty, newST) = typeOfListPure st pos (zip lstTypes $ fmap hasPosition items)
  put newST
  case ty of
    Left err -> throwError err
    Right ty' -> return ty'

typeOfIf :: Expr -> BlockOfCode -> BlockOfCode -> Collect GeneralType
typeOfIf cond thenBlock elseBlock
  | isBlockOfCodeEmpty thenBlock = throwError $ EmptyBlock (hasPosition thenBlock)
  | isBlockOfCodeEmpty elseBlock = throwError $ EmptyBlock (hasPosition elseBlock)
  | otherwise = do
    condType <- typeOf cond
    thenType <- typeOfSeq (blockOfCodeToExprs thenBlock)
    elseType <- typeOfSeq (blockOfCodeToExprs elseBlock)
    modify (addConstraint $ Constraint (hasPosition cond) condType (ConcreteType BoolType'))
    modify (addConstraint $ Constraint (hasPosition thenBlock) thenType elseType)
    return thenType

typeOfBinding :: BNFC'Position -> Name -> Expr -> Collect GeneralType
typeOfBinding pos varName expr1 = do
  scopes <- gets getScopes
  typeOfExpr <- typeOf expr1
  case getFromContextOrError pos varName scopes of
    Left _ -> do
      modify (addToContext varName (Variable varName typeOfExpr))
      return $ ConcreteType UnitType'
    Right oldCV ->
      if typeOfCV == typeOfExpr
        then do
          modify (addToContext varName (Variable varName typeOfExpr))
          return $ ConcreteType UnitType'
        else case typeOfExpr of
          tv@(TypeVar _ _) -> do
            modify (addToContext varName (Variable varName typeOfExpr))
            modify (addConstraint $ Constraint pos typeOfCV tv)
            return $ ConcreteType UnitType'
          _ -> throwError $ TypeMismatch pos typeOfCV typeOfExpr
      where
        typeOfCV = typeOfContextValue oldCV

typeOfFuncCall :: BNFC'Position -> [GeneralType] -> [(GeneralType, BNFC'Position)] -> GeneralType -> Collect GeneralType
typeOfFuncCall pos expectedTypes actualTypes retType =
  if length expectedTypes == length actualTypes
    then do
      mapM_ checkTypeMatchAndCollectConstraintForFuncCall $ zip expectedTypes actualTypes
      return retType
    else throwError $ ArityMismatch pos

createStruct :: BNFC'Position -> Name -> [CommaExpr] -> Collect GeneralType
createStruct pos name args = do
  st <- get
  let scopes = getScopes st
  case getFromContextOrError pos name scopes of
    Right cv -> case cv of
      Struct _ fields -> do
        computedTypes <- mapM typeOf (commaExprToExpr args)
        let xs = zip (generalTypeFromType <$> typesFromStructFields fields) (zip computedTypes $ fmap hasPosition fields)
        mapM_ checkTypeMatchAndCollectConstraint xs
        return $ DefinedType' name
      Alias _ ty -> case ty of
        DefinedType' aliasedTypeName -> createStruct pos aliasedTypeName args
        _ -> throwError $ NonStruct' pos
      _ -> throwError $ NonStruct' pos
    _ -> throwError $ NonStruct' pos

typeOfBuiltin :: BuiltinCall -> Collect GeneralType
typeOfBuiltin bc = case bc of
  Head _ ex -> isList ex
  Tail _ ex -> do
    ty <- isList ex
    return $ ListType' ty
  Cons pos ex ex' -> do
    prependType <- typeOf ex
    innerlistType <- isList ex'
    modify (addConstraint $ Constraint pos prependType innerlistType)
    return $ ListType' innerlistType
  Plus _ ex ex' -> isNumeric ex ex'
  Minus _ ex ex' -> isNumeric ex ex'
  Times _ ex ex' -> isNumeric ex ex'
  Divide _ ex ex' -> isNumeric ex ex'
  Equal pos ex ex' -> do
    t1 <- typeOf ex
    t2 <- typeOf ex'
    modify (addConstraint $ Constraint pos t1 t2)
    return $ ConcreteType BoolType'
  Less _ ex ex' -> do
    _ <- isNumeric ex ex'
    return $ ConcreteType BoolType'
  Greater _ ex ex' -> do
    _ <- isNumeric ex ex'
    return $ ConcreteType BoolType'
  Not _ ex -> do
    isBool [ex]
    return $ ConcreteType BoolType'
  And _ ex ex' -> do
    isBool [ex, ex']
    return $ ConcreteType BoolType'
  Or _ ex ex' -> do
    isBool [ex, ex']
    return $ ConcreteType BoolType'
  StringContains _ ex1 ex2 -> do
    isString [ex1, ex2]
    return $ ConcreteType BoolType'
  EmptyList _ ty -> return $ ListType' $ generalTypeFromType ty
  Trim _ s -> do
    isString [s]
    return $ ConcreteType StringType'
  Lower _ s -> do
    isString [s]
    return $ ConcreteType StringType'
  StringStartsWith _ ex1 ex2 -> do
    isString [ex1, ex2]
    return $ ConcreteType BoolType'
  DropFromStringStart _ s i -> do
    let es = [s, i]
    types <- typeOfBlock es
    checkTypeMatch (zip types $ fmap hasPosition es) [ConcreteType StringType', ConcreteType IntType']
    return $ ConcreteType StringType'
  IsListEmpty _ lst -> do
    _ <- isList lst
    return $ ConcreteType BoolType'
  IntToDouble _ i -> do
    isInt [i]
    return $ ConcreteType DoubleType'
  IntToString _ i -> do
    isInt [i]
    return $ ConcreteType StringType'
  DoubleToString _ d -> do
    isDouble [d]
    return $ ConcreteType StringType'

typeOfIOBuiltin :: IOBuiltinCall -> Collect GeneralType
typeOfIOBuiltin iobc = case iobc of
  GetLine _ -> return $ ConcreteType StringType'
  Print _ ex -> do
    _ <- typeOf ex
    return $ ConcreteType UnitType'
  PrintLn _ ex -> typeOfIOBuiltin $ Print Nothing ex

-- Helpers for functions for extracting types from specific expressions

typeOfFuncDef :: BNFC'Position -> Maybe Name -> [FuncArg] -> [Expr] -> GeneralType -> Collect GeneralType
typeOfFuncDef pos fname args body retType
  | null body = throwError $ EmptyBlock pos
  | hasDuplicates (fmap nameFromFuncArg args) = throwError $ ConflictingArgumentNamesError pos
  | otherwise = do
    argTypes <- mapM generalTypeFromFuncArg args
    let typings' = zipWith Typing' (fmap nameFromFuncArg args) argTypes
    addFuncItselfToContext fname typings' retType
    st <- get
    let oldScopes = getScopes st -- remember scopes before entering into function
    addLocalsToContext typings'
    lastExprType <- typeOfSeq body
    st' <- get
    put st' {getScopes = oldScopes}
    addRetTypeConstraint retType (lastExprType, hasPosition $ last body)
    return $ FuncType' argTypes lastExprType

addLocalsToContext :: [Typing''] -> Collect ()
addLocalsToContext args = do
  modify addNewScope
  mapM_ addTypingIntoContext args
  where
    addTypingIntoContext :: Typing'' -> Collect ()
    addTypingIntoContext (Typing' n t) = modify (addToContext n (Variable n t))

normalizeGT :: GeneralType -> Collect GeneralType
normalizeGT gt = case gt of
  FuncType' gts gt' -> do
    args <- mapM normalizeGT gts
    retType <- normalizeGT gt'
    return $ FuncType' args retType
  ListType' gt' -> do
    innerType <- normalizeGT gt'
    return $ ListType' innerType
  DefinedType' s -> do
    scopes <- gets getScopes
    case getFromContextOrError Nothing s scopes of
      Left err -> throwError err
      Right cv -> return $ typeOfContextValue cv
  _ -> return gt

addFuncItselfToContext :: Maybe Name -> [Typing''] -> GeneralType -> Collect ()
addFuncItselfToContext Nothing _ _ = return ()
addFuncItselfToContext (Just name) args retType =
  modify (addToContext name (Function args retType))

-- returns inner type of list
isList :: Expr -> Collect GeneralType
isList ex = do
  ty <- typeOf ex
  case ty of
    TypeVar _ _ -> do
      ftv <- takeFreshTV (hasPosition ex)
      modify (addConstraint $ Constraint (hasPosition ex) ty $ ListType' ftv)
      return ftv
    ListType' gt -> return gt
    _ -> throwError $ NonListType (hasPosition ex) ty

isNumeric :: Expr -> Expr -> Collect GeneralType
isNumeric ex ex' = do
  types <- typeOfBlock [ex, ex']
  let ts = [IntType Nothing, DoubleType Nothing]
  checkConcreteTypeMatch (hasPosition ex) types [[x, y] | x <- ts, y <- ts]
  let d = ConcreteType DoubleType'
  let i = ConcreteType IntType'
  return $ if d `elem` types then d else i

isBool :: [Expr] -> Collect ()
isBool = isTypeSeriesOf (BoolType Nothing)

isDouble :: [Expr] -> Collect ()
isDouble = isTypeSeriesOf (DoubleType Nothing)

isInt :: [Expr] -> Collect ()
isInt = isTypeSeriesOf (IntType Nothing)

isString :: [Expr] -> Collect ()
isString = isTypeSeriesOf (StringType Nothing)

isTypeSeriesOf :: Type -> [Expr] -> Collect ()
isTypeSeriesOf ty exprs = do
  types <- typeOfBlock exprs
  _ <- checkTypeMatch (zip types $ fmap hasPosition exprs) $ repeat (generalTypeFromType ty)
  return ()

checkTypeMatch :: [(GeneralType, BNFC'Position)] -> [GeneralType] -> Collect ()
checkTypeMatch actual expected =
  mapM_ checkTypeMatchAndCollectConstraint $ zip expected actual

checkConcreteTypeMatch :: BNFC'Position -> [GeneralType] -> [[Type]] -> Collect ()
checkConcreteTypeMatch pos actual expected =
  if or $ fmap ((and . zipWith (==) actual) . fmap generalTypeFromType) expected
    then return ()
    else throwError $ TypeMismatchMany pos (fmap (fmap generalTypeFromType) expected) actual

-- Constraint collecting

checkTypeMatchAndCollectConstraint :: (GeneralType, (GeneralType, BNFC'Position)) -> Collect ()
checkTypeMatchAndCollectConstraint (expected, (actual, pos))
  | expected == actual = return ()
  | isContainsTypeVars actual = do
    modify (addConstraint $ Constraint pos expected actual)
    return ()
  | otherwise = throwError $ TypeMismatch pos expected actual

checkTypeMatchAndCollectConstraintForFuncCall :: (GeneralType, (GeneralType, BNFC'Position)) -> Collect ()
checkTypeMatchAndCollectConstraintForFuncCall (expected, (actual, pos))
  | not (isContainsTypeVars actual) && not (isContainsTypeVars expected) =
    if actual == expected
      then return ()
      else throwError $ TypeMismatch pos expected actual
  | otherwise = do
    modify (addConstraint $ Constraint pos expected actual)
    return ()

addRetTypeConstraint :: GeneralType -> (GeneralType, BNFC'Position) -> Collect ()
addRetTypeConstraint expected (actual, pos) =
  if not (isContainsTypeVars expected)
    && not (isContainsTypeVars actual)
    && expected /= actual
    then throwError $ TypeMismatch pos expected actual
    else do
      modify (addConstraint $ Constraint pos actual expected)
      return ()

-- Helpers

generalTypeFromFuncArg :: FuncArg -> Collect GeneralType
generalTypeFromFuncArg la = case la of
  Typed _ (Typing _ _ t) -> normalizeGT (generalTypeFromType t)
  Typeless pos _ -> takeFreshTV pos
