{-# OPTIONS_GHC -Wall #-}

module AID2.Infer.Context where

import AID2.Data
import AID2.Infer.Data
import AID2.Scope (addToScopes, emptyScope, scopeFromList)
import qualified Data.Set as Set
import Normal.Abs
import Control.Monad.State
import AID2.Infer.Helpers

predef :: Context
predef =
  scopeFromList
    [ ("Bool", PredefinedType $ ConcreteType BoolType'),
      ("Int", PredefinedType $ ConcreteType IntType'),
      ("String", PredefinedType $ ConcreteType StringType'),
      ("Double", PredefinedType $ ConcreteType DoubleType')
    ]

freshTypeVars :: [Name]
freshTypeVars = do
  d <- ([1 ..] :: [Int])
  c <- ['a' .. 'z']
  return (c : show d)

addConstraint :: Constraint -> ST -> ST
addConstraint c st@(ST {getConstraints = constraints, getConstrainedTypeVars = constrainedTVs}) =
  st {getConstraints = c : constraints, getConstrainedTypeVars = Set.union (Set.fromList $ typeVarsFromConstraint c) constrainedTVs}

addToContext :: Name -> ContextValue -> ST -> ST
addToContext name ty' st@(ST {getScopes = scopes}) =
  st {getScopes = addToScopes name ty' scopes}

addNewScope :: ST -> ST
addNewScope st@(ST {getScopes = scopes}) = st {getScopes = emptyScope : scopes}

takeTV :: GeneralType -> ST -> ST
takeTV
  tv
  st@( ST
         { getFreshTypeVars = ftvs,
           getUsedTypeVars = usedTVs
         }
       ) = do
    st
      { getFreshTypeVars = drop 1 ftvs,
        getUsedTypeVars = Set.insert tv usedTVs
      }

startState :: ST
startState = ST [predef] [] freshTypeVars Set.empty Set.empty

takeFreshTV :: BNFC'Position -> Collect GeneralType
takeFreshTV pos = do
  ftvs <- gets getFreshTypeVars
  let ftv = TypeVar pos (head ftvs)
  modify (takeTV ftv)
  return ftv
