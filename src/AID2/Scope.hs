{-# OPTIONS_GHC -Wall #-}

module AID2.Scope where

import AID2.Data
import Data.Map.Lazy
import Prelude hiding (lookup)
import Normal.Abs

type Scope v = Map Name v

getFromContextOrError :: BNFC'Position -> Name -> [Scope v] -> Either Error v
getFromContextOrError pos name scopes = case getFromContext name scopes of
  Nothing -> Left $ Undefined pos name
  Just v -> Right v

getFromContext :: Name -> [Scope v] -> Maybe v
getFromContext _ [] = Nothing
getFromContext name (c : cx) = case lookup name c of
  Nothing -> getFromContext name cx
  Just x -> Just x

addToScopes :: Name -> v -> [Scope v] -> [Scope v]
addToScopes name cv [] = [insert name cv empty]
addToScopes name cv (c : cx) = insert name cv c : cx

addToScope :: Name -> v -> Scope v -> Scope v
addToScope = insert

inScopes :: Name -> [Scope v] -> Bool
inScopes name scopes = case lookupInScopes name scopes of
  Nothing -> False
  Just _ -> True

lookupInScopes :: Name -> [Scope v] -> Maybe v
lookupInScopes _ [] = Nothing
lookupInScopes name (c : cs) = case lookup name c of
  Nothing -> lookupInScopes name cs
  r@(Just _) -> r

scopeFromList :: [(Name, v)] -> Scope v
scopeFromList = fromList

emptyScope :: Scope v
emptyScope = empty

emptyScopes :: [Scope v]
emptyScopes = [emptyScope]
