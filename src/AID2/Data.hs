{-# LANGUAGE DeriveGeneric #-}
{-# OPTIONS_GHC -Wall #-}

module AID2.Data where

import GHC.Generics
import Generic.Random
  ( genericArbitrary,
    genericArbitraryRec,
    uniform,
    withBaseCase,
  )
import Normal.Abs
import Test.QuickCheck

type Name = String

type Error = Error' BNFC'Position

data Error' a
  = Error a String
  | Undefined a Name
  | UndefinedStruct a Name
  | NonStruct a Expr
  | NonStruct' a
  | ArityMismatch a
  | NonFunction a Expr
  | NonFunction' a
  | CannotInfer a GeneralType
  | UndefinedFieldAccess a Expr Name
  | CannotRedefine a Name
  | TypeDoesNotExist a Type
  | ContainsConflictingNamesError a Name
  | ConflictingArgumentNamesError a
  | ContainsUndefinedTypesError a Name
  | OccursCheck a GeneralType GeneralType
  | EmptyListError a Expr
  | NonListType a GeneralType
  | EmptyBlock a
  | NonListExpr a Expr
  | TypeError a GeneralType
  | TypeErrorMany a [GeneralType]
  | ZeroDivisionError a Expr
  | TypeMismatch a GeneralType GeneralType
  | TypeMismatchMany a [[GeneralType]] [GeneralType]
  deriving (Show, Eq)

data Type''
  = StringType'
  | BoolType'
  | UnitType'
  | IntType'
  | DoubleType'
  deriving (Show, Eq, Generic, Ord)

instance Arbitrary Type'' where
  arbitrary = genericArbitrary uniform

data GeneralType
  = ConcreteType Type''
  | TypeVar BNFC'Position Name
  | FuncType' [GeneralType] GeneralType
  | ListType' GeneralType
  | DefinedType' Name
  deriving (Show, Generic, Ord)

instance Eq GeneralType where
  (==) (TypeVar _ tv1) (TypeVar _ tv2) = tv1 == tv2
  (==) (ConcreteType t1) (ConcreteType t2) = t1 == t2
  (==) (FuncType' a1 r1) (FuncType' a2 r2) = a1 == a2 && r1 == r2
  (==) (ListType' i1) (ListType' i2) = i1 == i2
  (==) (DefinedType' n1) (DefinedType' n2) = n1 == n2
  (==) _ _ = False

instance Arbitrary GeneralType where
  arbitrary = do
    gt <- genericArbitraryRec uniform `withBaseCase` return (ConcreteType IntType')
    case gt of
      TypeVar _ s -> return $ TypeVar Nothing s
      x -> return x

intType :: GeneralType
intType = ConcreteType IntType'

doubleType :: GeneralType
doubleType = ConcreteType BoolType'

stringType :: GeneralType
stringType = ConcreteType StringType'

unitType :: GeneralType
unitType = ConcreteType UnitType'

boolType :: GeneralType
boolType = ConcreteType BoolType'

true :: Expr' BNFC'Position
true = ConstTrue Nothing

false :: Expr' BNFC'Position
false = ConstFalse Nothing

unit :: Expr' BNFC'Position
unit = ConstUnit Nothing

numericTypes :: [GeneralType]
numericTypes = [intType, doubleType]
