# Info

This repository contains interpreter for statically typed semi-functional programming language AID2 written on Haskell.

# Requirements

- stack >=2.7.5
- GHC >=9.0.2
- alex >= 3.2.7.1
- happy >= 1.20.0
- BNFC >= 2.9.4
- make

# Project structure

- *examples* - contains an example code written on AID2.
- *src*
  - *AID2* - contains different modules of interpreter: typechecker, interpreter, helpers.
  - *Library* - contains standard library of AID2.
  - *Syntax* - contains syntax for AID2. (BNF converter tool is used to generate parser and abstract syntax automatically.)
  - *Main.hs* - interpeter entry point.
- *tests* - contains tests for typechecker and interpreter.

# How to use

## Building the project:

<code>make build</code>

This will generate an executable *./target/aid2* that you can use as an interpreter for AID2.

It accepts one input parameter - file with code written on AID2.

Example: <code>./target/aid2 examples/example.aid2</code>

## Running tests:

<code>make build && make language_tests</code>

This runs all tests under *./tests* directory.

## Run in docker

<code>make build_in_docker</code>

<b>NOTE</b>: this will build the image `aid2_build:latest` and run the container with name `aid2_build`. If you have image and/or container with these names, pay attention to it so as not to lose your data.

## Documentation generation

<code>make latex</code> to get syntax documentation in *.tex* file

or

<code>make pdf</code> to get syntax documentation in *.pdf* file
